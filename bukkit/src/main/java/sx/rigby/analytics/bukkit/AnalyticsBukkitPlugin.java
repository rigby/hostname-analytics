package sx.rigby.analytics.bukkit;

import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.PluginManager;
import sx.rigby.analytics.bukkit.impl.AnalyticsBukkitCommand;
import sx.rigby.analytics.bukkit.impl.AnalyticsBukkitListener;
import sx.rigby.analytics.common.AnalyticsPlugin;

import java.io.File;

public class AnalyticsBukkitPlugin extends AnalyticsPlugin {
    private final AnalyticsBukkitBootstrap bootstrap;

    public AnalyticsBukkitPlugin(AnalyticsBukkitBootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    protected void registerCommand() {
        PluginCommand pluginCommand = bootstrap.getCommand("ha");
        if (pluginCommand != null) {
            pluginCommand.setExecutor(new AnalyticsBukkitCommand(this));
        }
    }

    @Override
    protected void registerEvent() {
        PluginManager pm = bootstrap.getServer().getPluginManager();
        pm.registerEvents(new AnalyticsBukkitListener(this), bootstrap);
    }

    @Override
    public File getDataFolder() {
        return bootstrap.getDataFolder();
    }
}
