package sx.rigby.analytics.bukkit.util;

import org.bukkit.ChatColor;
import org.checkerframework.checker.nullness.qual.NonNull;

public final class MessageParser {
    private MessageParser() {

    }

    public static @NonNull String parse(@NonNull String value) {
        return ChatColor.translateAlternateColorCodes('&', value);
    }
}
