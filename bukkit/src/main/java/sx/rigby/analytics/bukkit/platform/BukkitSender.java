package sx.rigby.analytics.bukkit.platform;

import org.bukkit.command.CommandSender;
import org.checkerframework.checker.nullness.qual.NonNull;
import sx.rigby.analytics.bukkit.util.MessageParser;
import sx.rigby.analytics.common.platform.Sender;

public final class BukkitSender implements Sender {
    private final CommandSender commandSender;

    public BukkitSender(@NonNull CommandSender commandSender) {
        this.commandSender = commandSender;
    }

    @Override
    public boolean hasPermission(@NonNull String permission) {
        return commandSender.hasPermission(permission);
    }

    @Override
    public void message(@NonNull String message) {
        commandSender.sendMessage(MessageParser.parse(message));
    }
}
