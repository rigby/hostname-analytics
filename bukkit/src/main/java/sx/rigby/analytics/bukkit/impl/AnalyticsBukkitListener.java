package sx.rigby.analytics.bukkit.impl;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.checkerframework.checker.nullness.qual.NonNull;
import sx.rigby.analytics.bukkit.platform.BukkitEventWrapper;
import sx.rigby.analytics.common.AnalyticsPlugin;
import sx.rigby.analytics.common.event.EventExecutor;

public class AnalyticsBukkitListener implements Listener {
    private final EventExecutor eventExecutor;

    public AnalyticsBukkitListener(@NonNull AnalyticsPlugin plugin) {
        this.eventExecutor = plugin.getEventExecutor();
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        eventExecutor.execute(new BukkitEventWrapper(event));
    }
}
