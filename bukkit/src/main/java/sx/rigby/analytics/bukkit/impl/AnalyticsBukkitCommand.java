package sx.rigby.analytics.bukkit.impl;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.checkerframework.checker.nullness.qual.NonNull;
import sx.rigby.analytics.bukkit.platform.BukkitSender;
import sx.rigby.analytics.common.AnalyticsPlugin;
import sx.rigby.analytics.common.command.CommandHandler;

public class AnalyticsBukkitCommand implements CommandExecutor {
    private final CommandHandler commandHandler;

    public AnalyticsBukkitCommand(@NonNull AnalyticsPlugin plugin) {
        this.commandHandler = plugin.getCommandHandler();
    }

    @Override
    public boolean onCommand(@NonNull CommandSender commandSender, @NonNull Command command, @NonNull String label, @NonNull String[] args) {
        commandHandler.handle(new BukkitSender(commandSender), args);
        return true;
    }
}
