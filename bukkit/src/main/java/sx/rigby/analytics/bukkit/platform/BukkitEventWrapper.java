package sx.rigby.analytics.bukkit.platform;

import org.bukkit.event.player.PlayerLoginEvent;
import org.checkerframework.checker.nullness.qual.NonNull;
import sx.rigby.analytics.common.platform.EventWrapper;
import sx.rigby.analytics.common.util.StringUtil;

import java.util.UUID;

public final class BukkitEventWrapper implements EventWrapper {
    private final PlayerLoginEvent playerLoginEvent;

    public BukkitEventWrapper(@NonNull PlayerLoginEvent playerLoginEvent) {
        this.playerLoginEvent = playerLoginEvent;
    }

    @Override
    public UUID uuid() {
        return playerLoginEvent.getPlayer().getUniqueId();
    }

    @Override
    public String hostname() {
        return StringUtil.parse(playerLoginEvent.getHostname());
    }
}
