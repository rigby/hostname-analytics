package sx.rigby.analytics.common.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class AsyncHelper {
    private AsyncHelper() {

    }

    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    public static ExecutorService executor() {
        return executorService;
    }
}
