package sx.rigby.analytics.common.util;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.regex.Pattern;

public final class StringUtil {
    private StringUtil() {

    }

    private static final Pattern PATTERN = Pattern.compile(":");

    /**
     * Removes the port attached to a hostname.
     */
    public static @NonNull String parse(@NonNull String string) {
        return PATTERN.split(string)[0];
    }
}
