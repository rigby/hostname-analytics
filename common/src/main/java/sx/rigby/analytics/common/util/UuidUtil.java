package sx.rigby.analytics.common.util;

import java.nio.ByteBuffer;
import java.util.UUID;

public final class UuidUtil {
    private UuidUtil() {

    }

    public static byte[] wrap(UUID uuid) {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
        buffer.putLong(uuid.getMostSignificantBits());
        buffer.putLong(uuid.getLeastSignificantBits());
        return buffer.array();
    }
}