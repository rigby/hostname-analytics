package sx.rigby.analytics.common.command;

import org.checkerframework.checker.nullness.qual.NonNull;
import sx.rigby.analytics.common.AnalyticsPlugin;
import sx.rigby.analytics.common.platform.Sender;
import sx.rigby.analytics.common.storage.DataStorage;
import sx.rigby.analytics.common.util.AsyncHelper;
import sx.rigby.analytics.common.util.WrappedValue;

import java.util.concurrent.CompletableFuture;

import static java.lang.String.format;

public final class CommandHandler {
    private static final String COMMAND_PERMISSION = "analytics.use";
    private static final String NO_PERMISSION_MESSAGE = "You do not have permission to execute this command.";
    private static final String COMMAND_USAGE_MESSAGE = "/ha <hostname>";
    private static final String COMMAND_OUTPUT_MESSAGE = "Stats for %s\nUnique: %s\nTotal: %s";

    private final DataStorage dataStorage;

    public CommandHandler(@NonNull AnalyticsPlugin plugin) {
        this.dataStorage = plugin.getDataStorage();
    }

    public void handle(Sender sender, String[] args) {
        if (!sender.hasPermission(COMMAND_PERMISSION)) {
            sender.message(NO_PERMISSION_MESSAGE);
            return;
        }

        if (args.length != 1) {
            sender.message(COMMAND_USAGE_MESSAGE);
            return;
        }

        String hostname = args[0];
        CompletableFuture<WrappedValue> future = CompletableFuture.supplyAsync(
                () -> new WrappedValue(dataStorage.getUnique(hostname), dataStorage.getTotal(hostname)), AsyncHelper.executor()
        );
        future.thenAccept((result) -> sender.message(format(COMMAND_OUTPUT_MESSAGE, hostname, result.getUnique(), result.getTotal())));
    }
}
