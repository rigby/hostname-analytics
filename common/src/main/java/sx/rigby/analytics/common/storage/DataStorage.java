package sx.rigby.analytics.common.storage;

import java.util.UUID;

public interface DataStorage {
    boolean init();

    void close();

    void insert(String hostname, UUID uniqueId);

    int getUnique(String hostname);
    int getTotal(String hostname);
}
