package sx.rigby.analytics.common.util;

public final class WrappedValue {
    private final int unique;
    private final int total;

    public WrappedValue(int unique, int total) {
        this.unique = unique;
        this.total = total;
    }

    public int getUnique() {
        return unique;
    }

    public int getTotal() {
        return total;
    }
}
