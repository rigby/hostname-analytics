package sx.rigby.analytics.common;

import sx.rigby.analytics.common.command.CommandHandler;
import sx.rigby.analytics.common.event.EventExecutor;
import sx.rigby.analytics.common.storage.DataStorage;
import sx.rigby.analytics.common.storage.impl.HikariStorage;
import sx.rigby.analytics.common.util.AsyncHelper;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AnalyticsPlugin {
    private final Logger log = Logger.getLogger(getClass().getName());

    protected DataStorage dataStorage;
    protected CommandHandler commandHandler;
    protected EventExecutor eventExecutor;

    protected abstract void registerCommand();
    protected abstract void registerEvent();

    public abstract File getDataFolder();

    public final void enable() {
        log.log(Level.INFO, "Initializing plugin...");

        dataStorage = new HikariStorage(this);
        if (!dataStorage.init()) {
            log.log(Level.SEVERE, "Failed to set up data storage.");
            return;
        }

        commandHandler = new CommandHandler(this);
        eventExecutor = new EventExecutor(this);

        log.log(Level.INFO, "Enabling plugin...");
        registerCommand();
        registerEvent();

        log.log(Level.INFO, "Plugin loaded.");
    }

    public final void disable() {
        log.log(Level.INFO, "Disabling plugin...");
        dataStorage.close();
        AsyncHelper.executor().shutdownNow();
        log.log(Level.INFO, "Plugin disabled.");
    }

    public DataStorage getDataStorage() {
        return dataStorage;
    }

    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

    public EventExecutor getEventExecutor() {
        return eventExecutor;
    }
}
