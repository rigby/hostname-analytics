package sx.rigby.analytics.common.storage.impl;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariPool;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.h2.jdbcx.JdbcDataSource;
import sx.rigby.analytics.common.AnalyticsPlugin;
import sx.rigby.analytics.common.storage.DataStorage;
import sx.rigby.analytics.common.util.UuidUtil;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.String.format;

public class HikariStorage implements DataStorage {
    private static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS `server_joins` (" +
                    "hostname VARCHAR(32) NOT NULL, " +
                    "uuid BINARY(16) NOT NULL, " +
                    "join_count INT NOT NULL, " +
                    "PRIMARY KEY (hostname, uuid))";
    private static final String GET_UNIQUE =
            "SELECT COUNT(*) from `server_joins` WHERE hostname = ?";
    private static final String GET_TOTAL =
            "SELECT SUM(join_count) FROM `server_joins` WHERE hostname = ?";
    private static final String INSERT =
            "INSERT INTO `server_joins` " +
                    "(hostname, uuid, join_count) VALUES(?, ?, 1) " +
                    "ON DUPLICATE KEY UPDATE join_count = join_count + 1";


    private HikariDataSource hikariDataSource;
    private final File dataFolder;
    private final Logger log = Logger.getLogger(getClass().getName());

    public HikariStorage(@NonNull AnalyticsPlugin plugin) {
        this.dataFolder = plugin.getDataFolder();
    }

    @Override
    public boolean init() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSourceClassName(JdbcDataSource.class.getName());
        hikariConfig.addDataSourceProperty("URL", "jdbc:h2:" + dataFolder.getAbsolutePath() +
                "/database;MODE=MySQL");

        try {
            hikariDataSource = new HikariDataSource(hikariConfig);
        } catch (HikariPool.PoolInitializationException e) {
            log.log(Level.SEVERE, "Unable to connect to the plugin primary database.", e);
        }

        try (Connection conn = hikariDataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(CREATE_TABLE)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Unable to create plugin primary database table.", e);
        }

        return hikariDataSource.isRunning();
    }

    @Override
    public void close() {
        if (hikariDataSource != null && hikariDataSource.isRunning()) {
            hikariDataSource.close();
        }
    }

    @Override
    public void insert(String hostname, UUID uniqueId) {
        try (Connection conn = hikariDataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(INSERT)) {
            stmt.setString(1, hostname);
            stmt.setBytes(2, UuidUtil.wrap(uniqueId));
            stmt.execute();
        } catch (SQLException e) {
            log.log(Level.SEVERE, format("Unable to insert join for %s on hostname %s.", uniqueId, hostname), e);
        }
    }

    @Override
    public int getUnique(String hostname) {
        return get(hostname, GET_UNIQUE);
    }

    @Override
    public int getTotal(String hostname) {
        return get(hostname, GET_TOTAL);
    }

    private int get(String hostname, String statement) {
        try (Connection conn = hikariDataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(statement)) {
            stmt.setString(1, hostname);
            try (ResultSet set = stmt.executeQuery()) {
                if (set.next()) return set.getInt(1);
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, format("Unable to retrieve data for %s.", hostname), e);
        }
        return 0;
    }
}
