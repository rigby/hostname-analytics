package sx.rigby.analytics.common.event;

import org.checkerframework.checker.nullness.qual.NonNull;
import sx.rigby.analytics.common.AnalyticsPlugin;
import sx.rigby.analytics.common.platform.EventWrapper;
import sx.rigby.analytics.common.storage.DataStorage;
import sx.rigby.analytics.common.util.AsyncHelper;

public final class EventExecutor {
    private final DataStorage dataStorage;

    public EventExecutor(@NonNull AnalyticsPlugin plugin) {
        this.dataStorage = plugin.getDataStorage();
    }

    public void execute(EventWrapper wrapper) {
        AsyncHelper.executor().submit(() -> dataStorage.insert(wrapper.hostname(), wrapper.uuid()));
    }
}
