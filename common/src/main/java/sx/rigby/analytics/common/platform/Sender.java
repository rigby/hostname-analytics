package sx.rigby.analytics.common.platform;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface Sender {

    boolean hasPermission(@NonNull String permission);

    void message(@NonNull String message);
}
